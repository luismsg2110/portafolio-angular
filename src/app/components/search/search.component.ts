import { ProductosService } from '../../services/productos.service';
import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent {

  termino:string = undefined;

  constructor( public route:ActivatedRoute ,
                public _ps:ProductosService) {

    route.params.subscribe( parametros => {
      this.termino = parametros['param'];
      
      _ps.search_product ( this.termino );

    });
   }

}
