
export { BlogComponent } from './blog/blog.component';
export { ContactComponent } from './contact/contact.component';
export { SearchComponent } from './search/search.component';
export { AboutComponent } from "./about/about.component";
export { PortafolioComponent } from "./portafolio/portafolio.component";
export { ItemComponent } from "./item/item.component";

