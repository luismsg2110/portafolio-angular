import { ProductosService } from '../../services/productos.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styles: []
})
export class ItemComponent{

  producto:any = undefined;
  id:string = undefined;

  constructor( private route:ActivatedRoute,
               private _ps:ProductosService){

    route.params.subscribe( parametros => {
      
      _ps.load_product ( parametros ['id'] )
      .subscribe ( res => {
        this.producto = res.json();
        this.id = parametros ['id'];
      })

    })
  }

}
