import { InformationService } from '../../services/information.service';
import { Component} from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styles: []
})
export class BlogComponent{
  id:string = undefined;
  constructor(public _is:InformationService) { 
    this.id = this._is.post['code'];
  }

}
