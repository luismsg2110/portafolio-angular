import { promise } from 'protractor';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { resolve } from 'path';

@Injectable()
export class ProductosService {
  productos:any[] = [];
  loading:boolean = true;
  producto_filtro:any[] = [];

  constructor( private http:Http ) {
    this.load_products();
   }

  public search_product ( termino:string ){

    if ( this.productos.length === 0 ) {
      this.load_products().then( ()=>{
      this.filtra_producto(termino);
      });
    }else{
      this.filtra_producto(termino);
    }
  }
  
  private filtra_producto (termino:string){
    this.producto_filtro = [];
    termino = termino.toLowerCase();
    this.productos.forEach ( prod => {
      if ( prod.categoria.indexOf(termino) >= 0 || prod.titulo.toLowerCase().indexOf(termino) >= 0 ) {
        this.producto_filtro.push(prod);
      }
    });
  }

  public load_product ( cod:string) { 
    return this.http.get (`https://portafolio-angular.firebaseio.com/productos/${ cod }.json`);
  }
  
  public load_products (){

    this.loading = true;
    
    let promise = new Promise (( resolve, reject )=>{
      this.http.get("https://portafolio-angular.firebaseio.com/productos_idx.json")
      .subscribe ( data => {
        this.loading = false;
        this.productos = data.json();
        resolve();
      }); 
    });
    
    return promise;
    
  }

}
