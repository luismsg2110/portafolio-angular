import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class InformationService {

  info:any = {};
  post:any[] = [];
  loaded:boolean = false;
  loaded_about:boolean = false;
  loaded_post:boolean = false;
  team:any[] = [];

  constructor( public http:Http) { 
    this.load_info();
    this.load_about();
    this.load_post();
  }

  public load_info(){
    this.http.get("assets/data/info.pagina.json")
      .subscribe( data => {
        this.loaded = true;
        this.info = data.json();
      })
  }

  public load_post(){
    this.http.get("assets/data/info.post.json")
      .subscribe( data => {
        this.loaded_post = true;
        this.post = data.json();
      })
  }

  public load_about () {
    this.http.get("https://portafolio-angular.firebaseio.com/about.json")
      .subscribe( data => {
        this.loaded_about = true;
        this.team = data.json();
      })
  }

}
