import { RouterModule, Routes } from '@angular/router';

import {
    AboutComponent,
    ItemComponent,
    PortafolioComponent,
    SearchComponent,
    ContactComponent,
    BlogComponent
} from "./components/index.paginas";

const app_routes: Routes = [
    {path: 'home', component: PortafolioComponent},
    {path: 'about', component: AboutComponent},
    {path: 'contact', component: ContactComponent},
    {path: 'blog', component: BlogComponent},
    {path: 'item/:id', component: ItemComponent},
    {path: 'search/:param', component: SearchComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const app_routing = RouterModule.forRoot(app_routes, {useHash:true});