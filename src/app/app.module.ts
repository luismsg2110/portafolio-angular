import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs';

//Servicios
import { InformationService } from "./services/information.service";
import { ProductosService } from './services/productos.service';
import { AgmCoreModule } from '@agm/core';

//Componentes
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { PortafolioComponent } from './components/portafolio/portafolio.component';
import { AboutComponent } from './components/about/about.component';
import { ItemComponent } from './components/item/item.component';
import { SearchComponent } from './components/search/search.component';
import { ContactComponent } from './components/contact/contact.component';
import { BlogComponent } from './components/blog/blog.component';

//Rutas
import { app_routing } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PortafolioComponent,
    AboutComponent,
    ItemComponent,
    SearchComponent,
    ContactComponent,
    BlogComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    app_routing,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAL5dHCl6wtYSbWIP6Es1S2oamFIQQHRsg'
    })
  ],
  providers: [
    InformationService,
    ProductosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
